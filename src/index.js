
const mc = require('minecraft-protocol');
const Vec3 = require('vec3');
const server = mc.createServer({
    'online-mode': false,   // optional
    encryption: false,      // optional
    host: '0.0.0.0',       // optional
    port: 25565,           // optional
    version: false,
    maxPlayers: 1000,
    motd: `A fallback server. How are you even here?`,
    hideErrors: false
});

const is116 = (version) => [753, 751, 736, 735, 754].includes(version);
const is115 = (version) => [573, 575, 578].includes(version);

server.socketServer.on('listening', () => {
    console.log(`${new Date().toTimeString()} - Started fallback server on ${server.socketServer.address().address}:${server.socketServer.address().port}!`);
});

const data = require('minecraft-data')('1.16.2');

server.on('connection', (client) => {
    console.log(`${new Date().toTimeString()} - Incoming connection from ${client.socket.address().address}:${client.socket.address().port}`)

    client.on('error', err => {
        console.error(err.stack);
    });
});

server.on('login', function(client) {
    const login = data.loginPacket;
    const Chunk = require('prismarine-chunk')(client.version);

    console.log(`${new Date().toTimeString()} - Player ${client.username}/${client.socket.address().address}:${client.socket.address().port} has been sent to Limbo.`);

    client.once('end', (reason) => {
        console.log(`${new Date().toTimeString()} - Player ${client.username}/${client.socket.address().address}:${client.socket.address().port} has been disconnected from Limbo with reason: ${reason}`); 
    });

    client.write('login', {
        entityId: client.id,
        gameMode: 1,
        dimension: !is116(client.protocolVersion) ? -1 : {
            "type": "compound",
            "name": "",
            "value": {
                "bed_works": {
                    "type": "byte",
                    "value": 1
                },
                "has_ceiling": {
                    "type": "byte",
                    "value": 0
                },
                "coordinate_scale": {
                    "type": "double",
                    "value": 1
                },
                "piglin_safe": {
                    "type": "byte",
                    "value": 0
                },
                "has_skylight": {
                    "type": "byte",
                    "value": 1
                },
                "ultrawarm": {
                    "type": "byte",
                    "value": 0
                },
                "infiniburn": {
                    "type": "string",
                    "value": "minecraft:infiniburn_overworld"
                },
                "effects": {
                    "type": "string",
                    "value": "minecraft:the_end"
                },
                "has_raids": {
                    "type": "byte",
                    "value": 1
                },
                "ambient_light": {
                    "type": "float",
                    "value": 0
                },
                "logical_height": {
                    "type": "int",
                    "value": 256
                },
                "natural": {
                    "type": "byte",
                    "value": 1
                },
                "respawn_anchor_works": {
                    "type": "byte",
                    "value": 0
                }
            }
        },
        difficulty: !is116(client.protocolVersion) ? 0 : undefined,
        maxPlayers: server.maxPlayers,
        levelType: "default",
        reducedDebugInfo: true,

        viewDistance: is115(client.protocolVersion) || is116(client.protocolVersion) ? 8 : undefined,
        hashedSeed: is115(client.protocolVersion) || is116(client.protocolVersion) ? [0, 0] : undefined,
        enableRespawnScreen: is115(client.protocolVersion) || is116(client.protocolVersion) ? false : undefined,
        previousGameMode: is116(client.protocolVersion) ? 255 : undefined,
        worldNames: is116(client.protocolVersion) ? login.worldNames : undefined,
        dimensionCodec: is116(client.protocolVersion) ? login.dimensionCodec : undefined,
        worldName: is116(client.protocolVersion) ? "minecraft:overworld" : undefined,
        isDebug: is116(client.protocolVersion) ? false : undefined,
        isFlat: is116(client.protocolVersion) ? false : undefined,
        isHardcore: false
    });

    client.write('position', {
        x: 8.5,
        y: 3,
        z: 8.5,
        yaw: 0,
        pitch: 0,
        flags: 0x00
    });

    const chunk = new Chunk();
    chunk.setBlockType(new Vec3.Vec3(8, 1, 8), 1);
    chunk.setSkyLight(new Vec3.Vec3(8, 1, 8), 15);

    client.write('map_chunk', {
        x: 0,
        z: 0,
        groundUp: true,
        bitMap: chunk.getMask(),
        chunkData: chunk.dump(),
        blockEntities: [],

        // 1.16 data
        ignoreOldData: true,
        heightmaps: { // Fake heightmap, no idea wtf this is for
            type: 'compound',
            name: '',
            value: {
                MOTION_BLOCKING: { type: 'longArray', value: new Array(36).fill([0, 0]) }
            }
        },
        biomes: chunk.dumpBiomes != undefined ? chunk.dumpBiomes() : undefined
    });
});

server.on('error', err => {
    console.error(err.stack);
});

const stdin = process.openStdin();

stdin.on('data', d => {
    const i = d.toString();
    const command = i.trim().split(/ /g)[0].toLowerCase();
    const args = i.trim().split(/ /g).slice(1);

    switch (command) {
        case 'list': {
            console.log(`Players currently online:\n${Object.keys(server.clients).length == 0 ? '(empty)' : server.playerCount <= 50 ? Object.keys(server.clients).map(a => '-' + server.clients[a].username).join('\n') : 'Too many players.'}\n\nPlayer count : ${server.playerCount} / ${server.maxPlayers}`);
            break;
        }

        case 'stop': {
            console.log(`${new Date().toTimeString()} - Shutting down fallback server...`);

            Object.keys(server.clients).forEach(id => {
                const client = server.clients[id];

                client.end(`Server closed.`);
            });

            server.close();
            process.exit(0);
        }
    }
});

process.on('uncaughtException', (err) => {
    console.error(`${new Date().toTimeString()} - An uncaught exception has occurred : ${err.stack}`);
});

process.on('unhandledRejection', (reason, p) => {
    console.error(`${new Date().toTimeString()} - An unhandled rejection has occurred : ${reason.stack} with promise ${p}`);
});
